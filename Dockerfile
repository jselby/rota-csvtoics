FROM alpine:latest

RUN echo "@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk upgrade --update && \
    apk add --no-cache python3 py3-pandas@testing && \
    pip3 install icalendar datetime uuid 

RUN touch /main.py && \
    touch /settings.py && \
    touch /input.csv && \
    touch /output.ics

CMD [ "python3", "/main.py" ]


