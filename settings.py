# dictionay containing shift type as key (case insensitive) and shift name/ start time/ end time as values
SHIFTS = {'Off': ['Off', '', ''],
          'Day': ['Day shift', '08:00', '17:00'],
          'LD': ['Long shift', '08:00', '20:00'],
          'N': ['Night shift', '20:00', '08:00']
          }

# address of hospital
ADDRESS = "A Hospital, Hospital Road, London, AB1 C11"

# used to generate calendar name
ROTA_TYPE = "Anaesthetics"
SHORT_LOCATION = "Hospital"

