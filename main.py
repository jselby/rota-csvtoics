import datetime
import icalendar
import pandas as pd
import settings
import uuid

# import df and variables
base_df = pd.read_csv('input.csv', header=None)

shifts = settings.SHIFTS
address = settings.ADDRESS
rota_type = settings.ROTA_TYPE
short_location = settings.SHORT_LOCATION

# convert all keys in shifts[] to lower case
shifts =  {k.lower(): v for k, v in shifts.items()}

# initialise calendar
cal = icalendar.Calendar()

# ics headers
cal.add('prodid', 'Rota csv to ics')
cal.add('version', '2.0')
cal.add('X-WR-TIMEZONE', 'Europe/London')
cal.add('X-WR-CALNAME', f"{rota_type} {short_location}")
cal.add('CALSCALE', 'GREGORIAN')
cal.add('METHOD', 'PUBLISH')

for row in range(len(base_df)):

# initialise variables and icalendar.event
    shift_date = base_df.iloc[row][0]
    csv_shift_name = base_df.iloc[row][1].lower()
    shift_name = shifts[csv_shift_name][0]
    start_date_time = f"{shift_date} {shifts[csv_shift_name][1]}"
    end_date_time = f"{shift_date} {shifts[csv_shift_name][2]}"
    event = icalendar.Event()

    if shift_name[:3].lower() == 'off':
        start_dt = datetime.datetime.strptime(shift_date, '%d/%m/%y')
        end_dt = datetime.datetime.strptime(shift_date, '%d/%m/%y')
    else:
        start_dt = datetime.datetime.strptime(start_date_time, '%d/%m/%y %H:%M')
        end_dt = datetime.datetime.strptime(end_date_time, '%d/%m/%y %H:%M')
        event.add('location', address)

# if night shift or off run to next day
    if shift_name.lower() in ['night', 'off']:
        end_dt += datetime.timedelta(days=1)

# create icalendar events
    event.add('summary', shift_name)
    event.add('dtstart', start_dt)
    event.add('dtstamp', datetime.datetime.now())
    event.add('uid', f"{uuid.uuid4().hex}@{rota_type}.{short_location}")
    event.add('dtend', end_dt)

# write calendar
    cal.add_component(event)

with open('output.ics', 'wb') as outfile:
    outfile.write(cal.to_ical())

# print count of shift types
print(base_df[1].value_counts())
print("Success: converted csv to ics")
