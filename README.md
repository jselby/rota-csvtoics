# Convert csv file into an ics file

Takes a work rota in a csv format and converts it to an ics file. This can then be imported into a calendar, e.g. caldav/ nextcloud. 

See corresponding blog post for explanation of script: https://jpselby.co.uk/projects/rota-csvtoics/

## How to use

### Using docker (recommended)

1. Ensure docker is installed; install guide availiable at https://docs.docker.com/get-docker/
2. Clone repository

    $ git clone https://gitlab.com/jselby/rota-csvtoics

3. Edit `input.csv` and `settings.py` using the guide in the configuration section below
4. Execute 

    $ sh run.sh

6. Use the `Build Dockerfile` option on first run to build the docker container
7. `run.sh` will run the python script inside of the docker container and output the result to `output.ics`

### Local install

1. Install required python dependencies:

    $ pip install datetime icalendar pandas uuid

2. Clone repository

    $ git clone https://gitlab.com/jselby/rota-csvtoics

3. Edit `input.csv` and `settings.py` using the guide in the configuration section below
4. Run with:

    $ python3 main.py

5. The output result is stored in `output.ics`

> **Note:** python venv can also be used to deploy the script

## Configuration

### File: input.csv

CSV file should be in this format:

    dd/mm/yy,shift_type

> **Note:** The csv has no header row

For example:

`input.csv`

    14/01/20,Day 
    15/01/20,LD 
    16/01/20,Off 
    17/01/20,N 
    18/01/20,N


### File: settings.py

Edit file with rota details.

#### SHIFTS

A dictionary used to determine shift details. CSV shift_type as key and values: shift name to appear on calendar, shift start and end times.

    SHIFTS = {'shift_type_from_csv_file':['shift_name_for_calendar', 'start_time_format_hh:mm', 'end_time_format_hh:mm]}
  
  > **Note:** hh:mm should be in 24hr format

For example:

    SHIFTS = {'Off': ['Off', '', ''],
              'Day': ['Day shift', '08:00', '17:00'],
              'LD': ['Long shift', '08:00', '20:00'],
              'N': ['Night shift', '20:00', '08:00']
              }

> **Note:** Leave start time/ end time empty to create an 'all day' event, see 'Off' entry above

#### ADDRESS
Address for calendar location.

    ADDRESS = "A Hospital, Hospital Road, London, AB1 C11"

#### ROTA TYPE and SHORT CODE
Used to generate calendar name.

    ROTA_TYPE = "Anaesthetics"
    SHORT_LOCATION = "A_Hospital"

#### Time Zone

The script is set to generate a calendar with timezone `Europe/London`.

To change this edit `main.py` line 24 with desired timezone.
