#!/bin/sh

read -p "Build DOCKERFILE? y/n " -r REPLY
if [ $REPLY = "y" ]; then
  docker build -t local/rota .
fi

echo "Running python script in docker container: local/rota"
docker run -it --rm --name rota_parse \
    -v ${PWD}/main.py:/main.py \
    -v ${PWD}/settings.py:/settings.py \
    -v ${PWD}/input.csv:/input.csv \
    -v ${PWD}/output.ics:/output.ics \
    local/rota
